import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { Provider, rootStore } from "./models/Root";

import CharacterListScreen from "./screens/CharacterListScreen";
import CharacterSheetScreen from "./screens/CharacterSheet";

const App: React.FC = () => {
  return (
    <Provider value={rootStore}>
      <Router>
        <div className="m-3">
          <Switch>
            <Route path="/characters/:id">
              <CharacterSheetScreen />
            </Route>
            <Route path="/">
              <CharacterListScreen />
            </Route>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
