import React from "react";
import { Switch, Route, NavLink, useParams, useRouteMatch } from "react-router-dom";
import { observer } from "mobx-react-lite";
import AttributesScreen from "./AttributesScreen";
import SkillsScreen from "./SkillsScreen";
import EdgesScreen from "./EdgesScreen";
import HindrancesScreen from "./HindrancesScreen";
import EditableText from "../components/EditableText";
import {useMst} from "../models/Root";

const NoCharacterMessage = () => (
  <h1>No Character Selected</h1>
)

export const CharacterSheetScreen = observer(() => {
  const { sheets } = useMst();
  const { id } = useParams();
  const { path } = useRouteMatch();

  const sheet = sheets.find(s => s.id === id);

  const h1Classes = "text-2xl font-bold pt-5 pb-5 w-full max-w-md";

  if (!sheet) {
    return <NoCharacterMessage />
  }

  const onNameChange = (value: string) => sheet.setName(value);

  return (
      <div className="w-full">
        <NavBar />
        <div className="mt-3 mb-16 w-full flex flex-col justify-center items-center">
          <h1 className={h1Classes}>
            <EditableText
              className="inline-block w-full"
              value={sheet.name}
              onChange={onNameChange}
            />
          </h1>
          <Switch>
            <Route path={`${path}/skills`}>
              <SkillsScreen sheet={sheet} />
            </Route>
            <Route path={`${path}/hindrances`}>
              <HindrancesScreen sheet={sheet} />
            </Route>
            <Route path={`${path}/edges`}>
              <EdgesScreen sheet={sheet} />
            </Route>
            <Route path={`${path}/`}>
              <AttributesScreen sheet={sheet}/>
            </Route>
          </Switch>
        </div>
      </div>
  );
})

export default CharacterSheetScreen;

const activeStyle = {
  borderBottom: ".25em solid",
  borderTop: ".25em solid",
}

interface NavButtonProps {
  to: string
  label: string
}

const NavButton = (props: NavButtonProps) => (
  <li className="flex-grow">
    <NavLink
      activeStyle={activeStyle}
      exact={true}
      to={props.to}
      className="text-center pt-4 pb-4 outline-none inline-block w-full"
    >
      {props.label}
    </NavLink>
  </li>
)

const NavBar = () => {
  const { url } = useRouteMatch();
  return (
    <nav className="fixed bottom-0 left-0 w-full bg-gray-800">
      <ul className="flex justify-around items-center">
        <NavButton to="/" label="<<" />
        <NavButton to={`${url}`} label="Attributes" />
        <NavButton to={`${url}/skills`} label="Skills" />
        <NavButton to={`${url}/hindrances`} label="Hindrances" />
        <NavButton to={`${url}/edges`} label="Edges" />
      </ul>
    </nav>
  );
};
