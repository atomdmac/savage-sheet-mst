import { CharacterSheetType } from "../models/CharacterSheet";

export interface ScreenProps {
  sheet: CharacterSheetType
}

export default ScreenProps;;
