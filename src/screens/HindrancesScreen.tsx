import React from "react"
import { observer } from "mobx-react-lite";
import HindranceManager from "../components/HindranceManager";
import ScreenProps from "./ScreenProps";

export const HindrancesScreen = observer((props: ScreenProps) => {
  const { sheet } = props;

  return (
    <div className="w-full max-w-md">
      <HindranceManager sheet={sheet} />
    </div>
  )
});

export default HindrancesScreen;
