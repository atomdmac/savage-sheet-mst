import React from "react"
import { NavLink } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useMst, DEFAULT_CHARACTER_STATE } from "../models/Root";

export const CharacterListScreen = observer(() => {
  const store = useMst();
  const { sheets } = store;

  const activeLinkStyle = {
    borderBottom: "3px solid white"
  }

  const sheetEls = sheets.map(s => (
    <li key={s.id} className="m-0">
      <NavLink
        key={s.id}
        activeStyle={activeLinkStyle}
        to={`/characters/${s.id}`}
        className="inline-block w-full m-0 p-2"
      >
        {s.name}
      </NavLink>
    </li>
  ));

  const addCharacter = () => {
    store.addCharacter(DEFAULT_CHARACTER_STATE);
  };

  return (
    <div>
      <ul>
        { sheetEls }
      </ul>
      <div className="w-full fixed bottom-0 left-0 p-3">
        <button
          onClick={addCharacter}
          className="w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
        >
          Add Character
        </button>
      </div>
    </div>
  );
});

export default CharacterListScreen;
