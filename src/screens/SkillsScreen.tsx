import React from "react"
import { observer } from "mobx-react-lite";
import { TraitList } from "../components/TraitList";
import { FilterList, FilterListItem } from "../components/FilterList";
import { skills } from "../SkillsList";
import ScreenProps from "./ScreenProps";
import { SkillSnapshot, Skill, SkillType, TraitType } from "../models/Trait";

export const SkillsScreen = observer((props: ScreenProps) => {
  const { sheet } = props;

  const h2Classes = "text-1xl font-bold pt-5 pb-5"

  const skillsList: FilterListItem[] = skills
    .filter(s => !sheet.skills.find((ss: SkillType) => ss.name === s.name))
    .map(s => ({
      key: s.name,
      value: s.name + (s.isCore ? "*" : ""),
      data: s
    }));

  const onSelectSkill = (selected: FilterListItem) => {
    sheet.addSkill(selected.data as SkillSnapshot);
  }

  const onDeleteSkill = (skill: TraitType) => {
    sheet.removeSkill(skill as Skill)
  };

  const addCoreSkills = () => {
    // TODO: Fix typing on addCoreSkills callback (is currently `any`)
    skills.forEach((skill: any) => {
      if (skill.isCore && !sheet.hasSkill(skill.name)) {
        sheet.addSkill(skill);
      }
    });
  }

  return (
    <div className="w-full max-w-md">
      <h2 className={h2Classes}>Skills ({sheet.skillCost} Spent)</h2>
      <TraitList deletable={true} onDelete={onDeleteSkill} traits={sheet.skills} />
      <FilterList items={skillsList} onSelect={onSelectSkill} />
      <button
        onClick={addCoreSkills}
        className="mb-6 w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
      >Add Core Skills</button>
    </div>
  )
});

export default SkillsScreen;
