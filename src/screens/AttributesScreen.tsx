import React from "react"
import { observer } from "mobx-react-lite";
import { TraitList } from "../components/TraitList";
import ScreenProps from "./ScreenProps";

export const AttributesScreen = observer((props: ScreenProps) => {
  const { sheet } = props;

  const h2Classes = "text-1xl font-bold pt-5 pb-5"

  return (
    <div className="w-full max-w-md">
      <h2 className={h2Classes}>Attributes ({sheet.attributeCost} Spent)</h2>
      <TraitList traits={sheet.attributes} />
      <div className="flex">
        <span className="w-1/3"><strong>Pace</strong>: {sheet.pace}</span>
        <span className="w-1/3"><strong>Parry</strong>: {sheet.parry}</span>
        <span className="w-1/3"><strong>Toughness</strong>: {sheet.toughness}</span>
      </div>
    </div>
  )
});

export default AttributesScreen;
