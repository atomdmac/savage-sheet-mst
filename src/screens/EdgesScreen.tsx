import React from "react"
import { observer } from "mobx-react-lite";
import { EdgeType } from "../models/Edge";
import EditableText from "../components/EditableText";
import ScreenProps from "./ScreenProps";

export const EdgesScreen = observer((props: ScreenProps) => {
  const { sheet } = props;

  const addEdge = () => {
    sheet.addEdge({});
  };

  const removeEdge = (edge: EdgeType) => {
    sheet.removeEdge(edge);
  };

  const edgesEls = sheet.edges.map(e => (
    <li key={e.name} className="flex justify-between items-center mb-3">
      <EditableText onChange={(newName: string) => e.setName(newName)} value={e.name} />
      <button
        className="ml-3 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
        onClick={() => removeEdge(e)}>-
      </button>
    </li>
  ));

  return (
    <div className="w-full max-w-md">
      <header className="flex items-center mb-3">
        <h2 className="flex-grow">Edges ({sheet.edgeCost} / {sheet.hindranceCost})</h2>
          <button
            className="ml-3 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
            onClick={addEdge}>+
          </button>
      </header>
      <ul>
        { edgesEls }
      </ul>
    </div>
  );
});

export default EdgesScreen;
