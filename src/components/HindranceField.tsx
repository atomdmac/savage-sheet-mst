import React, { ChangeEvent } from "react"
import { observer } from "mobx-react-lite";
import EditableText from "./EditableText";
import { HindranceType, HindranceLevelType } from "../models/Hindrance";

export interface HindranceFieldProps {
  hindrance: HindranceType
  onDelete: (hindrance: HindranceType) => void
}

export const HindranceField = observer((props: HindranceFieldProps) => {
  const { hindrance, onDelete } = props;

  const onChangeName = (newName: string) => {
    hindrance.setName(newName);
  };

  const onChangeLevel = (e: ChangeEvent<HTMLSelectElement>) => {
    hindrance.setLevel(e.target.value as HindranceLevelType);
  };

  const onDeleteClick = () => {
    if (onDelete) {
      onDelete(hindrance);
    }
  }

  return (
    <div className="flex items-center mb-3">
      <EditableText
        className="flex-grow mr-1"
        inputClassName="form-input inline-block w-full"
        contentClassName="inline-block w-full"
        onChange={onChangeName}
        value={hindrance.name}
      />
      <select 
        value={hindrance.level}
        className="form-input"
        onChange={onChangeLevel}
      >
        <option value="minor">Minor</option>
        <option value="major">Major</option>
      </select>
      <button
        className="ml-3 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
        onClick={onDeleteClick}
      >-</button>
    </div>
  );
});

export default HindranceField;
