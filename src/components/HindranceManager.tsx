import React from "react"
import { observer } from "mobx-react-lite";
import HindranceList from "./HindranceList";
import { HindranceType } from "../models/Hindrance";
import { CharacterSheetType } from "../models/CharacterSheet";

export interface HindranceListProps {
  sheet: CharacterSheetType
}

export const HindranceManager = observer((props: HindranceListProps) => {
  const { sheet } = props;

  const addHindrance = () => {
    sheet.addHindrance({});
  };

  const removeHindrance = (hindrance: HindranceType) => {
    sheet.removeHindrance(hindrance);
  };

  return (
    <div>
      <header className="flex items-center mb-3">
        <h2 className="flex-grow">Hindrances ({sheet.hindranceCost} Points)</h2>
          <button
            className="ml-3 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
            onClick={addHindrance}>+
          </button>
      </header>
      <HindranceList hindrances={sheet.hindrances} onDelete={removeHindrance}/>
    </div>
  );
});

export default HindranceManager;
