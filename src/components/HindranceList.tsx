import React from "react"
import { observer } from "mobx-react-lite";
import HindranceField from "./HindranceField";
import { HindranceType } from "../models/Hindrance";

export interface HindranceListProps {
  hindrances: HindranceType[]
  onDelete: (hindrance: HindranceType) => void
}

export const HindranceList = observer((props: HindranceListProps) => (
  <ul>
    {props.hindrances.map(h => (
      <li><HindranceField onDelete={props.onDelete} hindrance={h} /></li>
    ))}
  </ul>
));

export default HindranceList;
