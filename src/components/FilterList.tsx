import React from "react";
import { useState, useRef } from "react";
import { Keys } from "../util/constants";
import { titleCase } from "../util/string";

export interface FilterListItem {
  key: string,
  value: string,
  data?: any,
}

export interface FilterListProps {
  items: FilterListItem[],
  onSelect?: (selection: FilterListItem) => void,
}

export const FilterList = (props: FilterListProps) => {
  const {items, onSelect} = props;
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedIndex, setSelectedIndex] = useState(0);
  const listEl = useRef({} as HTMLSelectElement); // Seems hacky but 🤷

  const itemOptions = items
    .filter((i: FilterListItem) => i.value.match(new RegExp(searchTerm)))
    .map((i: FilterListItem, index = 0) => (
      <option 
        key={i.key} data-data={i}
        selected={index === selectedIndex}
        value={i.value}
        className="pt-3 pr-3 pb-3 pl-3"
      >{titleCase(i.value)}
      </option>
    ));

  const onSearchTermChange = (e: any) => {
    setSelectedIndex(0);
    setSearchTerm(e.target.value);
  }

  const onChange = (selectedItem: FilterListItem | undefined) => {
    if (onSelect && selectedItem) {
      setSearchTerm("");
      onSelect(selectedItem);
    }
  }

  const selectedItemFromEl = (e: HTMLSelectElement) => items.find(i => i.value === e.value);
  const selectedItemFromEvent = (e: MouseEvent): FilterListItem | undefined => {
    if (e.target) return selectedItemFromEl(e.target as HTMLSelectElement);
    return undefined;
  };

  const onKeyPress = (e: any) => {
    switch(e.keyCode) {
      case Keys.UP:
        setSelectedIndex(selectedIndex + 1);
        break;
      case Keys.DOWN:
        setSelectedIndex(selectedIndex - 1);
        break;
      case Keys.ENTER:
        if (listEl.current) {
          onChange(selectedItemFromEl(listEl.current as HTMLSelectElement));
        }
    }
  }

  return (
    <div className="flex-col h-auto">
      <input
        type="text"
        placeholder="Search Skills"
        className="form-input w-full mb-3"
        value={searchTerm}
        onChange={onSearchTermChange}
        onKeyDown={onKeyPress}
      />
      <select
        ref={listEl}
        className="form-input w-full pl-0 pr-0 mb-3 object-responsive"
        size={Math.min(4, itemOptions.length)}
        onChange={(e: any) => onChange(selectedItemFromEvent(e))}
      >
        {itemOptions}
      </select>
    </div>
  );
}
