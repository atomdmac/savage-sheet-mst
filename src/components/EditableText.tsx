import React, { ChangeEvent, useEffect, useRef, useState, KeyboardEvent } from "react";
import { Keys } from "../util/constants";

export interface EditableTextProps {
  className?: string
  inputClassName?: string
  contentClassName?: string
  onChange: (value: string) => void
  value: string
}

export const EditableText = (props: EditableTextProps) => {
  const inputEl = useRef({} as HTMLInputElement); // Seems hacky but 🤷
  const [isEditing, setIsEditing] = useState(false);
  const [currentInput, setCurrentInput] = useState("");
  const onChange = (e: ChangeEvent<HTMLInputElement>) => setCurrentInput(e.target.value);
  const hasUnsavedChanges = currentInput && currentInput !== props.value;

  useEffect(() => {
    isEditing && inputEl.current.focus();
  });

  const startEdit = () => {
    if (!hasUnsavedChanges) {
      setCurrentInput(props.value);
    }
    setIsEditing(true);
  };

  const stopEdit = () => {
    setIsEditing(false);
  }

  const commitEdit = () => {
    stopEdit();
    props.onChange(currentInput);
    setCurrentInput("");
  }

  const onKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    switch(e.keyCode) {
      case Keys.ENTER:
        commitEdit();
    }
  }

  const inputClasses = props.inputClassName
    ? props.inputClassName
    : "form-input inline-block w-full";
  const contentClasses = props.contentClassName
    ? props.contentClassName
    : "inline-block w-full";

  return (
    <span className={props.className}>
      {isEditing &&
        <input
          onBlur={stopEdit}
          onKeyDown={onKeyDown}
          className={inputClasses}
          ref={inputEl}
          type="text"
          value={currentInput}
          onChange={onChange}
        />}
      {!isEditing
        && <span
          onClick={startEdit}
          className={contentClasses}
        >{props.value}
        {hasUnsavedChanges && !isEditing
          && <span >&nbsp;(unsaved)</span>}
        </span>}
    </span>
  );
}

export default EditableText;
