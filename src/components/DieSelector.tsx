import React from "react"
import { DiceType } from "../models/Dice";

export interface DieSelectorProps {
  onChange: (e: any) => void
  die: DiceType
}

export const DieSelector = (props: DieSelectorProps) => {
  const onChange = (e: any) => props.onChange(parseInt(e.target.value) as DiceType)
  return (
    <select className="form-input" value={props.die} onChange={onChange}>
      <option value="4">d4</option>
      <option value="6">d6</option>
      <option value="8">d8</option>
      <option value="10">d10</option>
      <option value="12">d12</option>
    </select>
  )
}

export default DieSelector;
