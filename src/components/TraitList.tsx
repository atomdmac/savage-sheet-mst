import React from "react"
import { observer } from "mobx-react-lite";
import TraitField from "./TraitField";
import { TraitType } from "../models/Trait";

export interface TraitListProps {
  traits: TraitType[]
  deletable?: boolean
  onDelete?: (trait: TraitType) => void
}

export const TraitList = observer((props: TraitListProps) => (
  <ul>
    {props.traits.map(t => (
      <li><TraitField onDelete={props.onDelete} trait={t} /></li>
    ))}
  </ul>
));

export default TraitField;
