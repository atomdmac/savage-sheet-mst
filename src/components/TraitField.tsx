import React, { ChangeEvent } from "react";
import { observer } from "mobx-react-lite";
import { DieSelector } from "./DieSelector";
import { TraitType } from "../models/Trait";
import { DiceType } from "../models/Dice";

export interface TraitFieldProps {
  onChange?: (e: any, trait: TraitType) => void
  trait: TraitType,
  onDelete?: (trait: TraitType) => void,
}

export const TraitField = observer((props: TraitFieldProps) => {
  const changeDieType = (d: DiceType) => {
    props.trait.setDieType(d)
  }

  const changeModifier = (e: ChangeEvent<HTMLInputElement>) => {
    props.trait.setModifier(parseInt(e.target.value, 10) as number);
  }

  const onDeleteClick = () => {
    if (props.onDelete) {
      props.onDelete(props.trait);
    }
  }

  return (
    <div className="flex items-center mb-3">
      <label className="flex-grow mr-1">{props.trait.displayName}:</label>
      <DieSelector onChange={changeDieType} die={props.trait.die} />
      <input
        className="form-input ml-3 w-16"
        type="number"
        value={props.trait.modifier}
        onChange={changeModifier}
      />
      <span className="ml-3 w-6">({props.trait.cost})</span>
      {props.onDelete &&
        <button
          onClick={onDeleteClick}
          className="ml-3 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
        >-</button>
      }
    </div>
  );
});

export default TraitField;
