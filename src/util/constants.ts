export enum Keys {
  ENTER = 13,
  ESC = 27,
  UP = 40,
  DOWN = 38,
}
