export const capitalize = (input: string) =>
  String.fromCharCode(input.charCodeAt(0) - 32) + input.substr(1);

export const titleCase = (input: string) => input
  .split(" ")
  .map((word: string) => capitalize(word))
  .join(" ");
