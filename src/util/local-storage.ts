import { RootSnapshotIn } from "../models/Root";

export const STATE_KEY = "SAVAGE_SHEET";

export const saveState = (snapshot: any) => {
  localStorage.setItem(STATE_KEY, JSON.stringify(snapshot))
}

export const loadState = (defaultState: RootSnapshotIn): RootSnapshotIn =>
  JSON.parse(localStorage.getItem(STATE_KEY) as string) || defaultState;
