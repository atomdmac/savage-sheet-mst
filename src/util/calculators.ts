import { SkillType, AttributeType } from "../models/Trait";

export const calculateSkillCost = (skill: SkillType, linkedAttribute: AttributeType) => {
  const baseCost = skill.isCore ? 0 : ((skill.die - 2) / 2);
  const bonusCost = Math.max(0, (skill.die - linkedAttribute.die)) / 2;

  return baseCost + bonusCost;
}

// export const calculateTotalSkillCost = (skills: SkillType[], attributes: AttributeType[]) => {
  // return skills.reduce((total: number, skill: TraitType) => {
    // return total + calculateSkillCost(attributeTraitTypes, skill)
  // }, 0)
// }

export const calculateTotalAttributeCost = (attributeTraitTypes: AttributeType[]) => {
  return attributeTraitTypes.reduce((total: number, attribute: AttributeType) => {
    return total + ((attribute.die - 4) / 2);
  }, 0);
}

