import { Instance, SnapshotIn, types } from "mobx-state-tree";

export const Edge = types
  .model("Edge")
  .props({
    name: types.optional(types.string, "New Edge"),
  })
  .views(self => ({
    get cost() {
      return 1;
    }
  }))
  .actions(self => ({
    setName(newName: string) {
      self.name = newName;
    },
  }));

export type EdgeType = Instance<typeof Edge>;
export interface Edge extends EdgeType {};
type EdgeSnapshotType = SnapshotIn<typeof Edge>;
export interface EdgeSnapshot extends EdgeSnapshotType {};
