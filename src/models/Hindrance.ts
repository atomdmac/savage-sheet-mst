import { Instance, SnapshotIn, types } from "mobx-state-tree";

export const HindranceLevel = types.union(
  types.literal("major"),
  types.literal("minor")
);

export type HindranceLevelType = Instance<typeof HindranceLevel>;

export const Hindrance = types
  .model("Hindrance")
  .props({
    name: types.optional(types.string, "New Hindrance"),
    level: types.optional(HindranceLevel, "minor"),
  })
  .views(self => ({
    get cost() {
      return self.level === "minor"
        ? 1
        : 2;
    }
  }))
  .actions(self => ({
    setName(newName: string) {
      self.name = newName;
    },
    setLevel(newLevel: HindranceLevelType) {
      self.level = newLevel;
    }
  }));

export type HindranceType = Instance<typeof Hindrance>;
export interface Hindrance extends HindranceType {};
type HindranceSnapshotType = SnapshotIn<typeof Hindrance>;
export interface HindranceSnapshot extends HindranceSnapshotType {};
