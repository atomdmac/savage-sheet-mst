import { Instance, SnapshotIn, types } from "mobx-state-tree";
import { Attribute, AttributeType, SkillModel, SkillType, SkillSnapshot } from "./Trait";
import { HindranceType, HindranceSnapshot, Hindrance } from "./Hindrance";
import { Edge, EdgeType, EdgeSnapshot } from "./Edge";
import { v4 as uuid } from "uuid";

export const CharacterSheetModel = types
  .model("CharacterSheet")
  .props({
    id: types.optional(
      types.identifier,
      uuid
    ),
    name: types.optional(types.string, ""),
    pace: types.optional(types.number, 6),
    isWildCard: types.optional(types.boolean, false),
    attributes: types.array(Attribute),
    skills: types.array(SkillModel),
    edges: types.array(Edge),
    hindrances: types.array(Hindrance),
  })
  .views(self => ({
    get attributeCost() {
      return self.attributes.reduce((count, a: AttributeType) => count + a.cost, 0);
    },
    get skillCost() {
      return self.skills.reduce((count, s: SkillType) => count + s.cost, 0);
    },
    get edgeCost() {
      return self.edges.reduce((count, h: EdgeType) => count + h.cost, 0);
    },
    get hindranceCost() {
      return self.hindrances.reduce((count, h: HindranceType) => count + h.cost, 0);
    },
    get parry() {
      const fighting = self.skills.find(s => s.name === "fighting")
      if (!fighting) {
        return 2;
      }
      return Math.floor((fighting.die + fighting.modifier) / 2) + 2;
    },
    get toughness() {
      const vigor = self.attributes.find(a => a.name === "vigor")
      if (!vigor) {
        return 2;
      }
      return Math.floor((vigor.die + vigor.modifier) / 2) + 2;
    },
    hasSkill(name: string) {
      return !!self.skills.find((s: SkillType) => s.name === name);
    }
  }))
  .actions(self => ({
    setName(newName: string) {
      self.name = newName;
    },
    addEdge(snapshot: EdgeSnapshot){
      self.edges.push(Edge.create(snapshot));
    },
    removeEdge(edge: EdgeType){
      self.edges.remove(edge);
    },
    addHindrance(snapshot: HindranceSnapshot){
      self.hindrances.push(Hindrance.create(snapshot));
    },
    removeHindrance(hindrance: HindranceType){
      self.hindrances.remove(hindrance);
    },
    addSkill(snapshot: SkillSnapshot) {
      const skill = SkillModel.create(snapshot);

      const linked = self.attributes.find(a => a.name === skill.linkedAttributeName);
      if (linked) {
        skill.setLinkedAttribute(linked);
      }

      self.skills.push(skill);
    },
    removeSkill(skill: SkillType) {
      self.skills.remove(skill);
    }
  }));

export type CharacterSheetType = Instance<typeof CharacterSheetModel>;
export interface CharacterSheet extends CharacterSheetType {};
type CharacterSheetSnapshotType = SnapshotIn<typeof CharacterSheetModel>;
export interface CharacterSheetSnapshot extends CharacterSheetSnapshotType {};
