import { useContext, createContext } from "react";
import { types, Instance, onSnapshot, SnapshotIn } from "mobx-state-tree";

import { CharacterSheetModel, CharacterSheetSnapshot, CharacterSheetType } from "./CharacterSheet";
import { loadState, saveState } from "../util/local-storage";

export const DEFAULT_CHARACTER_STATE: CharacterSheetSnapshot = {
  name: "Character Name",
  attributes: [{
    name: "agility",
    die: 4,
    modifier: 0,
  }, {
    name: "smarts",
    die: 4,
    modifier: 0,
  }, {
    name: "spirit",
    die: 4,
    modifier: 0,
  }, {
    name: "strength",
    die: 4,
    modifier: 0,
  }, {
    name: "vigor",
    die: 4,
    modifier: 0,
  }],
  skills: []
};

export const DEFAULT_STATE: RootSnapshotIn = {
  sheets: [DEFAULT_CHARACTER_STATE],
};

const RootModel = types
  .model({
    sheets: types.array(CharacterSheetModel),
  })
  .actions(self => ({
    addCharacter(sheet: CharacterSheetSnapshot) {
      self.sheets.push(CharacterSheetModel.create(sheet));
    },
    removeCharacter(character: CharacterSheetType) {
      self.sheets.remove(character);
    }
  }))

export const rootStore = RootModel.create(loadState(DEFAULT_STATE));

onSnapshot(rootStore, snapshot => console.log("Snapshot: ", snapshot));
onSnapshot(rootStore, snapshot => saveState(snapshot));

export type RootInstance = Instance<typeof RootModel>;
export type RootSnapshotIn = SnapshotIn<typeof RootModel>;
const RootStoreContext = createContext<null | RootInstance>(null);

export const Provider = RootStoreContext.Provider;
export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error("Store cannot be null, please add a context provider");
  }
  return store;
}
