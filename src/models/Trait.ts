import { Instance, types, SnapshotIn } from "mobx-state-tree";
import { Dice, DiceType } from "./Dice"
import { titleCase } from "../util/string";
import { v4 as uuid } from "uuid";

export const Trait = types
  .model("Trait")
  .props({
    name: types.string,
    die: Dice,
    modifier: types.optional(types.number, 0),
 })
 .views(self => ({
   get cost() {
     return 0;
   },
   get displayName() {
     return titleCase(self.name);
   }
 }))
 .actions(self => ({
   setDieType(d: DiceType) {
     self.die = d;
   },
   setModifier(m: number) {
     self.modifier = m;
   }
 }))

export type TraitType = Instance<typeof Trait>

export const AttributeName = types
  .union(
    types.literal("agility"),
    types.literal("smarts"),
    types.literal("spirit"),
    types.literal("strength"),
    types.literal("vigor"),
  );

export const Attribute = Trait
  .props({
    id: types.optional(
      types.identifier,
      () => `attribute-${uuid()}`
    ),
    name: AttributeName,
  })
  .views(self => ({
    get cost() {
      return ((self.die - 2) / 2) - 1;
    }
  }))

export type AttributeType = Instance<typeof Attribute>

export const SkillModel = Trait
  .props({
    isCore: types.boolean,
    linkedAttributeName: types.string,
    linkedAttribute: types.maybe(
      types.reference(Attribute)
    ),
  })
  .actions(self => ({
    setLinkedAttribute(attribute: AttributeType) {
      self.linkedAttribute = attribute;
    }
  }))
  .views(self => ({
    get cost() {
      const baseCost = ((self.die - 2) / 2) - (self.isCore ? 1 : 0);
      const bonusCost = self.linkedAttribute
        ? Math.max(0, (self.die - self.linkedAttribute.die)) / 2
        : 0;

      return baseCost + bonusCost;
    },
   get displayName() {
     return titleCase(self.name) + (self.isCore ? "*" : "");
   }
  }))

export type SkillType = Instance<typeof SkillModel>;
export interface Skill extends SkillType {};
type SkillSnapshotType = SnapshotIn<typeof SkillModel>;
export interface SkillSnapshot extends SkillSnapshotType {};
