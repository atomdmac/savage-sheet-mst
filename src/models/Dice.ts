import { Instance, types } from "mobx-state-tree";

export const Dice = types.union(
  types.literal(4),
  types.literal(6),
  types.literal(8),
  types.literal(10),
  types.literal(12),
)

export type DiceType = Instance<typeof Dice>
